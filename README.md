# Mundo Gratis

Mundo Gratis es un proyecto cuyo fin es nada menos de crear productos gratuitos.

Esto se lograría con una economía colaborativa de alta tecnología, con alto grado de automatización, basada en incentivos claros y en principios conocidos del mundo del software libre (FLOSS).

Conceptos claves son:
- Circuitos económicos cerrados y abiertos
- La unidad de producción gratuita - o Unidad de producción de coste marginal cero
- Commons Finance - financiamiento colectivo sustentable de bienes comunes

¡Pronto más!

# Free World (as in beer)

Free World (as in beer) is a project with a very ambitious goal: to create fee products (as in beer),

The idea is to combine high technology, automatization, peer production principles (like those of the FLOSS world) and clear incentives.

A couple of crucial concepts are:
- Closed and open circular economies
- The Free Production Unit - or Zero Marginal Cost Production Unit
- Commons Finance - long-term crowdfunding of common goods
 
More to come soon!

